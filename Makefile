APP=fl
BS=build-scripts
PREFIX=/usr/local
DESTDIR=

DEFAULT_PLANG=C
PLANG=$(DEFAULT_PLANG)
MULTILINGUAL=TRUE
ifeq ($(MULTILINGUAL),TRUE)
	PLANG_DIRNAME=lang_$(PLANG)
else
	PLANG_DIRNAME=
endif

ifeq ($(PLANG),D)
	DC=ldc2 #dmd, gdc or ldc2
	RELEASE_ARGS=release $(DC) dynamic
	DEBUG_ARGS=debug $(DC) dynamic
	SPEC_PKG_OPTIONS=$(PLANG) $(DC)
endif
ifeq ($(PLANG),C)
	CC=gcc
	RELEASE_ARGS=release $(CC)
	DEBUG_ARGS=debug $(CC)
	SPEC_PKG_OPTIONS=$(PLANG) $(CC)
endif

bin:
	$(BS)/$(PLANG_DIRNAME)/compile.sh $(RELEASE_ARGS)

debug:
	$(BS)/$(PLANG_DIRNAME)/compile.sh $(DEBUG_ARGS)

install:
	$(BS)/install.sh --install $(DESTDIR)$(PREFIX)

uninstall:
	$(BS)/install.sh --uninstall $(DESTDIR)$(PREFIX)

clean:
	rm -rf build/


