fl is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

---

## fl

*fl* is a small experimental analogue of the *ls* utility.

---

## Build from source

### Preparing

Before assembling, you need to install a compiler for C with Glib or D and [chrpath](https://directory.fsf.org/wiki/Chrpath).

For example, in Debian-based distributions, you can install required packages as follows:

`sudo apt install gcc libglib2.0-dev ldc chrpath`


### Compilation and installation

Creating of executable bin-file:

`make`

Also, you can choose a compiler for assembling:

`make CC=clang`

Installation (by default, main directory is /usr/local/):

`sudo make install`

After that, the application is ready for use.

You can install this application in any other directory:

`make install PREFIX=/home/$USER/sandbox`

Uninstall:

`sudo make uninstall`

If you installed in an alternate directory:

`make uninstall PREFIX=/home/$USER/sandbox`

You can choose an alternative programming language for the project. This program now has two implementations: on *D* and on *C*. Examples of usage:

`make PLANG=D`


---

