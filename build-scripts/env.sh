#!/bin/bash
APP=fl
readonly VERSION=`cat source/version`
ARCH=`dpkg --print-architecture`

readonly BINPATH_BASEDIR=build/bin
BINPATH="${BINPATH_BASEDIR}/${APP}"
#readonly SUMMARY=`cat pkg-info/summary`

readonly DEFAULT_ROOTDIR=/usr
readonly DEFAULT_ALTROOTDIR=/usr/local
# RL is "Relative Location"
readonly RL_BINDIR=bin
readonly RL_SHAREDIR=share
#readonly RL_HELPDIR=${RL_SHAREDIR}/help
#readonly RL_HELPDIR_EN=${RL_HELPDIR}/en_US/${APP}
#readonly RL_HELPDIR_RU=${RL_HELPDIR}/ru_RU/${APP}
#readonly RL_HELPDIR_EO=${RL_HELPDIR}/eo/${APP}
readonly RL_DOCDIR=${RL_SHAREDIR}/doc
readonly RL_DOCDIR_SPEC=${RL_SHAREDIR}/doc/${APP}
#readonly RL_MANDIR=${RL_SHAREDIR}/man
#readonly RL_MANDIR_EN=${RL_MANDIR}/man1
#readonly RL_MANDIR_RU=${RL_MANDIR}/ru/man1
#readonly RL_MANDIR_EO=${RL_MANDIR}/eo/man1
#readonly RL_BASHCOMP=${RL_SHAREDIR}/bash-completion/completions

#readonly RPMDIST_RAW=$( rpm --eval %dist )
#readonly RPMDIST=${RPMDIST_RAW:1} # not used
