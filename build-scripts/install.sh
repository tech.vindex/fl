#!/bin/bash -e
source build-scripts/env.sh
set -x

MODE="$1"
PREFIX="$2"

if [[ ${PREFIX} != "" ]]; then
    BASE="${PREFIX}"
else
    BASE=${DEFAULT_ALTROOTDIR}
fi

ALTBINDIR="${BASE}/${RL_BINDIR}"
ALTDOCDIR="${BASE}/${RL_DOCDIR}"
ALTDOCDIR_SPEC="${BASE}/${RL_DOCDIR_SPEC}"

echo "prefix:" ${BASE}
if [[ ${MODE} == "--install" ]]; then
    mkdir -p "${ALTBINDIR}" "${ALTDOCDIR_SPEC}"
    install "${BINPATH}" "${ALTBINDIR}/"
    cp copyright ${ALTDOCDIR_SPEC}/
else
    rm -f ${ALTBINDIR}/${APP}
    rm -rf ${ALTDOCDIR_SPEC}
fi


set +x
