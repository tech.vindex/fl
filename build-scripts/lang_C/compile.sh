#!/bin/bash -e
source build-scripts/env.sh
SRCDIR=source/lang_C
SRC="${SRCDIR}/main.c ${SRCDIR}/langlocal.c"
BINVERSION=$1   # release or debug
CC=$2           # gcc or clang
BINPATH="${BINPATH_BASEDIR}/${APP}"
mkdir -p ${BINPATH_BASEDIR}
DEBUG_OPTIONS="-g -O0"
OPTIM="-O2"

CFLAGS=`pkg-config --cflags glib-2.0`
LIBS=`pkg-config --libs glib-2.0`

set -x

if [[ $BINVERSION == release ]]; then
    ${CC} ${SRC} -o ${BINPATH} ${OPTIM} ${CFLAGS} ${LIBS}
    objcopy --strip-debug --strip-unneeded ${BINPATH} ${BINPATH}
else
    ${CC} ${SRC} -o ${BINPATH} ${DEBUG_OPTIONS} ${CFLAGS} ${LIBS}
fi

cd "${BINPATH_BASEDIR}"
    chrpath -d ${APP}
    chmod 755 ${APP}
cd -

set +x
