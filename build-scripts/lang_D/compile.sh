#!/bin/bash -e
source build-scripts/env.sh
SRCDIR=source/lang_D
SRC="${SRCDIR}/main.d ${SRCDIR}/langlocal.d"

BINVERSION=$1   # release or debug
DC=$2           # dmd, ldc2 or gdc
LINKING_MODE=$3 # dynamic or static
STATIC_OPTIONS=""

BINPATH="${BINPATH_BASEDIR}/${APP}"

if [[ ${LINKING_MODE} == static ]] ; then
    if [[ ${DC} == ldc2 ]] ; then
        STATIC_OPTIONS=-defaultlib=:libphobos2-ldc.a,:libdruntime-ldc.a
        LDC_VERSION=`ldc2 --version | head -n 1 \
            | sed "s/LDC - the LLVM D compiler (//" | sed "s/)://"`
        echo Version of LLVM D Compiler: ${LDC_VERSION}
        if [[ "${LDC_VERSION}" != "1.2.0" ]]
        then
            STATIC_OPTIONS+=" -link-defaultlib-shared=false -L-lz"
        fi
    elif [[ ${DC} == gdc ]] ; then
        STATIC_OPTIONS=-static-libphobos
    fi
fi

mkdir -p ${BINPATH_BASEDIR}


if [[ ${DC} == dmd ]] ; then
    DEBUG_OPTIONS="-debug -g"
elif [[ ${DC} == ldc2 ]] ; then
    DEBUG_OPTIONS="-d-debug -gc"
elif [[ ${DC} == gdc ]] ; then
    DEBUG_OPTIONS="-fdebug"
fi

OUT="-of"
OPTIM="-O -release"
if [[ ${DC} == gdc ]] ; then
    OUT="-o "
    OPTIM="-O2 -frelease"
fi


set -x

if [[ $BINVERSION == release ]] ; then
    ${DC} ${SRC} ${OUT}${BINPATH} ${OPTIM} -Jsource/ ${STATIC_OPTIONS}
    objcopy --strip-debug --strip-unneeded ${BINPATH} ${BINPATH}
else
    ${DC} ${SRC} ${OUT}${BINPATH} ${DEBUG_OPTIONS} -Jsource/ ${STATIC_OPTIONS}
fi

cd "${BINPATH_BASEDIR}"
    chrpath -d ${APP}
    chmod 755 ${APP}
cd -

set +x
