/*
 * Copyright (C) 2019-2020 Eugene 'Vindex' Stulin
 *
 * This file is a part of fl.
 * fl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdio.h>

static const char* currentLanguage = "en_US";
static size_t usedLanguageColumn = 0;

static char** langTable;
size_t langNR = 0;
size_t langNC = 0;

const char* getSystemLanguage() {
    char* langAndEncoding = getenv("LANG");
    char* lang = strdup(langAndEncoding);
    size_t i = 0;
    char c = lang[i];
    while(c != '\0') {
        if (c == '.') {
            lang[i] = '\0';
            break;
        }
        ++i;
        c = lang[i];
    }
    return lang;
}


const char* getCurrentLanguage() {
    return currentLanguage;
}


void chooseLanguage(const char* language) {
    if (langTable == NULL) return;
    for(size_t i = 0; i < langNC; ++i) {
        if (strcmp(langTable[i], language) != 0) continue;
        usedLanguageColumn = i;
        return;
    }
}


void initLocalization(char** table, size_t nc, size_t nr) {
    char* t[nr][nc];
    for (size_t r = 0; r < nr; r++) {
        for (size_t c = 0; c < nc; c++) {
            t[r][c] = table[r*nc + c];
        }
    }
    langTable = table;
    langNC = nc;
    langNR = nr;
    assert(strcmp(table[0], "en_US") == 0);
    chooseLanguage(getSystemLanguage());
}


const char* s_(const char* englishString) {
    const char* line;
    for(size_t i = 1; i < langNR; ++i) {
        if (0 == strcmp(langTable[i*langNC], englishString)) {
            return langTable[i*langNC + usedLanguageColumn];
        }
    }
    return (char*)englishString;
}
