/*
 * Copyright (C) 2019-2020 Eugene 'Vindex' Stulin
 *
 * This file is a part of fl.
 * fl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LANGLOCAL_H
#define LANGLOCAL_H

const char* getSystemLanguage();
const char* getCurrentLanguage();
void initLocalization(char** table, size_t nc, size_t nr);
void chooseLanguage(const char* language);
const char* s_(const char* englishString);

#endif //LANGLOCAL_H
