/*
 * Copyright (C) 2019-2020 Eugene 'Vindex' Stulin
 *
 * fl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <grp.h>
#include <pwd.h>
#include <time.h>
#include <sys/time.h>
#include <libgen.h>

#include <glib.h>

#include "langlocal.h"

typedef gboolean bool_t;

void getDirsAndFilesFromArray(const GArray* args, GArray* dirs, GArray* files);
void displayFileList(GArray* dirs, GArray* files, int details, int hidden);
GArray* makeArray();
void freeArray(GArray* arr);


int main(int argc, char* argv[]) {
    const size_t nc = 2, nr = 3;
    char* translations[3][2] = {
        "en_US", "ru_RU",
        "No such file or directory", "Нет такого файла или каталога",
        "cannot access", "невозможно получить доступ к"
    };

    initLocalization((char**)translations, nc, nr);
    bool_t hidden = FALSE, details = TRUE;
    GArray* dirs = makeArray();
    GArray* files = makeArray();
    char* currentDir = ".";
    if (argc == 1) {
        g_array_append_val(dirs, currentDir);
    } else {
        GArray* args = makeArray();
        for(size_t i = 0; i < argc; ++i) {
            if (strcmp(argv[i], "--hidden") == 0) {
                hidden = TRUE;
                continue;
            }
            g_array_append_val(args, argv[i]);
        }
        getDirsAndFilesFromArray(args, dirs, files);
        freeArray(args);
    }
    if (files->len == 0 && dirs->len == 0) {
        return 0;
    }
    displayFileList(dirs, files, details, hidden);

    freeArray(dirs);
    freeArray(files);

    return 0;
}


GArray* makeArray() {
    return g_array_new(FALSE, FALSE, sizeof(void*));
}
void freeArray(GArray* arr) {
    g_array_free(arr, TRUE);
}


bool_t isDir(const char* filepath) {
    struct stat st;
    lstat(filepath, &st);
    return (st.st_mode & S_IFMT) == S_IFDIR;
}


bool_t isSymlink(const char* filepath) {
    struct stat st;
    lstat(filepath, &st);
    return (st.st_mode & S_IFMT) == S_IFLNK;
}


bool_t exists(const char* filepath) {
    return access(filepath, 0) == 0;
}


gint compareStrings(gconstpointer a, gconstpointer b) {
    char* str1 = g_ascii_strup(*(char**)a, -1); //to upper
    char* str2 = g_ascii_strup(*(char**)b, -1);
    gint result = strcmp(str1, str2);
    free(str1);
    free(str2);
    return result;
}


void getDirsAndFilesFromArray(const GArray* args,
                              GArray* dirs,
                              GArray* files) {
    char* app = g_array_index(args, char*, 0);
    for(size_t i = 1; i < args->len; ++i) {
        char* arg = g_array_index(args, char*, i);
        if (!exists(arg)) {
            fprintf(
                stderr, "%s: %s '%s': %s\n",
                app, s_("cannot access"), arg, s_("No such file or directory")
            );
            continue;
        }
        if (isDir(arg)) {
            g_array_append_val(dirs, arg);
        } else {
            g_array_append_val(files, arg);
        }
    }
    g_array_sort(dirs, &compareStrings);
    g_array_sort(files, &compareStrings);
}


typedef enum {
    SOCKET,
    SYMLINK,
    REGULAR_FILE,
    BLOCK_DEVICE,
    DIRECTORY,
    CHAR_DEVICE,
    PIPE,
    WRONG_TYPE
} LinuxFileType;


LinuxFileType getLinuxFileTypeByMode(mode_t mode) {
    switch(mode & S_IFMT) {
        case S_IFSOCK: return SOCKET;
        case S_IFLNK:  return SYMLINK;
        case S_IFREG:  return REGULAR_FILE;
        case S_IFBLK:  return BLOCK_DEVICE;
        case S_IFDIR:  return DIRECTORY;
        case S_IFCHR:  return CHAR_DEVICE;
        case S_IFIFO:  return PIPE;
        default: return WRONG_TYPE;
    }
}


GString* getUnixModeLine(mode_t mode) {
    char permissions[11] = {'\0'};
    LinuxFileType type = getLinuxFileTypeByMode(mode);
    switch(type) {
        case SOCKET:       permissions[0] = 's'; break;
        case SYMLINK:      permissions[0] = 'l'; break;
        case REGULAR_FILE: permissions[0] = '-'; break;
        case BLOCK_DEVICE: permissions[0] = 'b'; break;
        case DIRECTORY:    permissions[0] = 'd'; break;
        case CHAR_DEVICE:  permissions[0] = 'c'; break;
        case PIPE:         permissions[0] = 'p'; break;
        default:           permissions[0] = 'z'; break;
    }

    int mask;
    for(int i = 0; i < 3; ++i) {
        switch(i) {
            case 0: mask = (mode & S_IRWXU) >> 6; break;
            case 1: mask = (mode & S_IRWXG) >> 3; break;
            case 2: mask = mode & S_IRWXO; break;
        }
        switch(mask) {
            case 0:  strcpy(&permissions[1+i*3], "---"); break;
            case 1:  strcpy(&permissions[1+i*3], "--x"); break;
            case 2:  strcpy(&permissions[1+i*3], "-w-"); break;
            case 3:  strcpy(&permissions[1+i*3], "-wx"); break;
            case 4:  strcpy(&permissions[1+i*3], "r--"); break;
            case 5:  strcpy(&permissions[1+i*3], "r-x"); break;
            case 6:  strcpy(&permissions[1+i*3], "rw-"); break;
            case 7:  strcpy(&permissions[1+i*3], "rwx"); break;
            default: strcpy(&permissions[1+i*3], "zzz"); break;
        }
    }
    return g_string_new(permissions);
}


GString* getNameByUID(uid_t uid) {
    struct passwd* p = getpwuid(uid);
    if (p == NULL) {
        char buffer[11] = {'\0'};
        sprintf(buffer, "%u", uid);
        return g_string_new(buffer);
    }
    return g_string_new((*p).pw_name);
}


GString* getNameByGID(uid_t gid) {
    struct group* p = getgrgid(gid);
    if (p == NULL) {
        char buffer[11] = {'\0'};
        sprintf(buffer, "%u", gid);
        return g_string_new(buffer);
    }
    char* name = (*p).gr_name;
    return g_string_new(name);
}


typedef enum {
    LEFT,
    RIGHT
} Alignment;


void alignLines(GArray* lines, Alignment alignment) {
    size_t maxLength = 0;
    for(size_t i = 0; i < lines->len; ++i) {
        size_t nsymbols = g_array_index(lines, GString*, i)->len;
        if (nsymbols > maxLength) maxLength = nsymbols;
    }
    for(size_t i = 0; i < lines->len; ++i) {
        GString* line = g_array_index(lines, GString*, i);
        size_t nsymbols = line->len;
        if (alignment == LEFT) {
            for(size_t j = 0; j < maxLength - nsymbols; ++j) {
                g_string_append(line, " ");
            }
        } else {
            for(size_t j = 0; j < maxLength - nsymbols; ++j) {
                g_string_prepend(line, " ");
            }
        }

    }
}
void alignRight(GArray* lines) {
    alignLines(lines, RIGHT);
}
void alignLeft(GArray* lines) {
    alignLines(lines, LEFT);
}


GString* getLineWithLinkNumber(nlink_t nlink) {
    char strnlink[21];
    sprintf(strnlink, "%zd", nlink);
    GString* links = g_string_new(strnlink);
    return links;
}


GString* getFileSizeLine(off_t size) {
    char strfilesize[21];
    sprintf(strfilesize, "%td", size);
    GString* sizeLine = g_string_new(strfilesize);
    return sizeLine;
}


GString* getMonthLine(struct tm* mtime) {
    const char* strmonth;
    switch(mtime->tm_mon+1) {
        case G_DATE_JANUARY:   strmonth = s_("Jan"); break;
        case G_DATE_FEBRUARY:  strmonth = s_("Feb"); break;
        case G_DATE_MARCH:     strmonth = s_("Mar"); break;
        case G_DATE_APRIL:     strmonth = s_("Apr"); break;
        case G_DATE_MAY:       strmonth = s_("May"); break;
        case G_DATE_JUNE:      strmonth = s_("Jun"); break;
        case G_DATE_JULY:      strmonth = s_("Jul"); break;
        case G_DATE_AUGUST:    strmonth = s_("Aug"); break;
        case G_DATE_SEPTEMBER: strmonth = s_("Sep"); break;
        case G_DATE_OCTOBER:   strmonth = s_("Oct"); break;
        case G_DATE_NOVEMBER:  strmonth = s_("Nov"); break;
        case G_DATE_DECEMBER:  strmonth = s_("Dec"); break;
    }
    GString* month = g_string_new(strmonth);
    return month;
}


GString* getDayLine(struct tm* mtime) {
    char strday[3];
    sprintf(strday, "%d", mtime->tm_mday);
    GString* day = g_string_new(strday);
    return day;
}


// get year or hours with minutes
GString* makePenultimateColumnLine(struct tm* mtime) {
    int hour = mtime->tm_hour;
    int min = mtime->tm_min;

    int year = mtime->tm_year + 1900;
    time_t ct = time(NULL); //current time
    struct tm* tmCT = gmtime(&ct);
    int currYear = tmCT->tm_year+1900;
    bool_t fileIsOld = year != currYear;

    char penColumnBuf[12] = {'\0'};
    if (fileIsOld) {
        sprintf(penColumnBuf, "%d", year);
    } else {
        sprintf(penColumnBuf, "%02d:%02d", hour, min);
    }
    return g_string_new(penColumnBuf);
}


GString* getFileLine(const char* filepath) {
    char* filename = basename((char*)filepath);
    char* result = strstr(filename, " ");
    bool_t spacesExists = result ? TRUE : FALSE;
    if (spacesExists) {
        char bufWithApostrophe[258] = {'\0'};
        bufWithApostrophe[0] = '\'';
        strcpy(bufWithApostrophe+1, filename);
        bufWithApostrophe[1+strlen(filename)] = '\'';
        filename = strdup(bufWithApostrophe);
    }
    GString* outname;
    if (isSymlink(filepath)) {
        char buf[260] = {'\0'};
        strcpy(buf, " -> ");
        int ret = readlink(filepath, buf+4, 256);
        char tempNameWithLink[260+256] = {'\0'};
        strcpy(tempNameWithLink, filename);
        strcat(tempNameWithLink, buf);
        outname = g_string_new(tempNameWithLink);
    } else {
        outname = g_string_new(filename);
    }
    if (spacesExists) {
        free(filename);
    }
    return outname;
}


void printDetailedInfo(GArray* files) {
    if (files->len == 0) return;
    GArray* modeArr = makeArray();
    GArray* nlinkArr = makeArray();
    GArray* ownerArr = makeArray();
    GArray* groupArr = makeArray();
    GArray* sizeArr = makeArray();
    GArray* monthArr = makeArray();
    GArray* dayArr = makeArray();
    GArray* penColumnArr = makeArray();
    GArray* filenameArr = makeArray();

    for(size_t i = 0; i < files->len; ++i) {
        char* filepath = g_array_index(files, char*, i);
        struct stat st;
        lstat(filepath, &st);
        tzset();
        time_t t = st.st_mtim.tv_sec - timezone;
        struct tm* mtime = gmtime(&t);

        GString* permissions = getUnixModeLine(st.st_mode);
        GString* links = getLineWithLinkNumber(st.st_nlink);
        GString* owner = getNameByUID(st.st_uid);
        GString* group = getNameByGID(st.st_gid);
        GString* size = getFileSizeLine(st.st_size);
        GString* month = getMonthLine(mtime);
        GString* day = getDayLine(mtime);
        GString* penultimateColumn = makePenultimateColumnLine(mtime);
        GString* outname = getFileLine(filepath);

        g_array_append_val(modeArr, permissions);
        g_array_append_val(nlinkArr, links);
        g_array_append_val(ownerArr, owner);
        g_array_append_val(groupArr, group);
        g_array_append_val(sizeArr, size);
        g_array_append_val(monthArr, month);
        g_array_append_val(dayArr, day);
        g_array_append_val(penColumnArr, penultimateColumn);
        g_array_append_val(filenameArr, outname);
    }

    alignRight(nlinkArr);
    alignLeft(ownerArr);
    alignLeft(groupArr);
    alignRight(sizeArr);
    alignRight(dayArr);
    alignRight(penColumnArr);

    for(size_t i = 0; i < filenameArr->len; ++i) {
        printf(
            "%s %s %s %s %s %s %s %s %s\n",
            g_array_index(modeArr, GString*, i)->str,
            g_array_index(nlinkArr, GString*, i)->str,
            g_array_index(ownerArr, GString*, i)->str,
            g_array_index(groupArr, GString*, i)->str,
            g_array_index(sizeArr, GString*, i)->str,
            g_array_index(monthArr, GString*, i)->str,
            g_array_index(dayArr, GString*, i)->str,
            g_array_index(penColumnArr, GString*, i)->str,
            g_array_index(filenameArr, GString*, i)->str
        );
    }

    size_t len = modeArr->len;
    for(size_t i = 0; i < len; ++i) {
        g_string_free(g_array_index(modeArr, GString*, i), TRUE);
        g_string_free(g_array_index(nlinkArr, GString*, i), TRUE);
        g_string_free(g_array_index(ownerArr, GString*, i), TRUE);
        g_string_free(g_array_index(groupArr, GString*, i), TRUE);
        g_string_free(g_array_index(sizeArr, GString*, i), TRUE);
        g_string_free(g_array_index(monthArr, GString*, i), TRUE);
        g_string_free(g_array_index(dayArr, GString*, i), TRUE);
        g_string_free(g_array_index(penColumnArr, GString*, i), TRUE);
        g_string_free(g_array_index(filenameArr, GString*, i), TRUE);
    }

    freeArray(modeArr);
    freeArray(nlinkArr);
    freeArray(ownerArr);
    freeArray(groupArr);
    freeArray(sizeArr);
    freeArray(monthArr);
    freeArray(dayArr);
    freeArray(penColumnArr);
    freeArray(filenameArr);
}


void printDirContent(const char* dir, int details, int hidden) {
    GArray* filelist = makeArray();
    DIR* dirstream = opendir(dir);
    struct dirent* direntEntry = readdir(dirstream);
    while(direntEntry) {
        if ((direntEntry->d_name)[0] != '.' || hidden) {
            char filename[256];
            strcpy(filename, direntEntry->d_name);
            char tempDir[256];
            strcpy(tempDir, dir);
            char* dirWithSlash = strcat(tempDir, "/");

            char* filepath = strdup(strcat(dirWithSlash, filename));
            g_array_append_val(filelist, filepath);
        }
        direntEntry = readdir(dirstream);
    }
    g_array_sort(filelist, &compareStrings);
    printDetailedInfo(filelist);
    for(size_t i = 0; i < filelist->len; ++i) {
        free(g_array_index(filelist, char*, i));
    }
    g_array_free(filelist, TRUE);
    closedir(dirstream);
}


void displayFileList(GArray* dirs, GArray* files, int details, int hidden) {
    gboolean outputDirName = TRUE;
    if (files->len == 0 && dirs->len == 1 || dirs->len == 0) {
        outputDirName = FALSE;
    }
    printDetailedInfo(files);
    if (files->len > 1 && outputDirName) {
        printf("\n");
    }
    for(size_t i = 0; i < dirs->len; ++i) {
        const char* d = g_array_index(dirs, char*, i);
        printDirContent(d, details, hidden);
        if (i != (*dirs).len-1) {
            printf("\n");
        }
    }
}
