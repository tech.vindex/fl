/*
 * Copyright (C) 2019-2020 Eugene 'Vindex' Stulin
 *
 * fl is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import
    std.algorithm, std.array, std.conv, std.datetime,
    std.getopt, std.path, std.process, std.file,
    std.stdio, std.string, std.range, std.typecons;

import core.stdc.stdlib;
import core.sys.posix.sys.stat;
import core.sys.posix.pwd;
import core.sys.posix.grp;
import core.sys.posix.time;

import langlocal;
       
private immutable translations = mixin(import("translations.dtxt").strip);

int main(string[] args) {
    initLocalization(translations);
    bool hidden = false, details = true;
    args.getopt(std.getopt.config.passThrough, "a|all", &hidden);
    string[] dirs, files;
    if (args.length > 1) {
        args.extractDirsAndFilesFromArray(dirs, files);
    } else {
        dirs ~= ".";
    }
    displayFileList(dirs, files, details, hidden);
    return 0;
}


void extractDirsAndFilesFromArray(string[] args,
                                  ref string[] dirs,
                                  ref string[] files) {
    string app = args[0];
    foreach(arg; args[1 .. $]) {
        if (!exists(arg)) {
            writefln(
                "%s: %s '%s': %s",
                app, "cannot access"._s, arg, "No such file or directory"._s
            );
            continue;
        }
        if (arg.isDir) {
            dirs ~= arg;
        } else {
            files ~= arg;
        }
    }
    dirs.sort!("toUpper(a) < toUpper(b)");
    files.sort!("toUpper(a) < toUpper(b)");
}


void displayFileList(string[] dirs, string[] files, bool details, bool hidden)
in {
    foreach(dir; dirs) assert(dir.isDir);
    foreach(f; files) assert(!f.isDir);
}
body {
    bool outputDirName = true;
    if (files.length == 0 && dirs.length == 1 || dirs.length == 0) {
        outputDirName = false;
    }
    printDetailedInfo(files);
    if (files.length > 1 && outputDirName) {
        writeln;
    }
    foreach(i, d; dirs) {
        if (outputDirName) {
            writeln(d.stripRight('/') ~ "/:");
        }
        printDirContent(d, details, hidden);
        if (i != dirs.length - 1) {
            writeln;
        }
    }
}


void printDirContent(string dir,
                     bool details = true,
                     bool hidden = false)
in {
    assert(dir.isDir);
}
body {
    string[] filelist = getFileList(dir, hidden);
    filelist.sort!("toUpper(a) < toUpper(b)");

    if (details) {
        printDetailedInfo(filelist);
        return;
    }
    foreach(f; filelist) {
        string outname = baseName(f);
        if (outname.canFind(' ')) {
            outname = "'" ~ outname ~ "'";
        }
        writeln(outname);
    }
}


struct FileItem {
    string permissions;
    string nlink;
    string owner;
    string group;
    string size;
    string month;
    string day;
    string penultimateColumn;
    string filename;
    void print() {
        writefln(
            "%s %s %s %s %s %s %s %s %s",
            permissions, nlink, owner, group, size,
            month, day, penultimateColumn,
            filename
        );
    }
}


void printDetailedInfo(string[] files) {
    FileItem[] fileItems;

    foreach(file; files) {
        FileItem item;
        
        FileStat st = FileStat(file);
        string mode = makeUnixFileModeLine(st);
        item.permissions = mode;

        item.nlink = st.nlink.to!string;
        item.owner = st.user.to!string;
        item.group = st.group.to!string;
        item.size = st.size.to!string;

        item.month = st.mtime.month.to!string.capitalize._s;
        item.day = st.mtime.day.to!string;

        if (Clock.currTime().year != st.mtime.year) {
            item.penultimateColumn = st.mtime.year.to!string;
        } else {
            auto t = format!"%02d:%02d"(st.mtime.hour, st.mtime.minute);
            item.penultimateColumn = t;
        }

        string outname = baseName(file);
        if (outname.canFind(' ')) {
            outname = "'" ~ outname ~ "'";
        }
        if (file.isSymlink) {
            string original = readLink(file);
            if (original.canFind(' ')) {
                original = "'" ~ original ~ "'";
            }
            outname ~= " -> " ~ original;
        }
        item.filename = outname;
        fileItems ~= item;
    }

    string[] nlinkArr, ownerArr, groupArr, sizeArr, dayArr, penColumnArr;
    foreach(item; fileItems) {
        nlinkArr ~= item.nlink;
        ownerArr ~= item.owner;
        groupArr ~= item.group;
        sizeArr ~= item.size;
        dayArr ~= item.day;
        penColumnArr ~= item.penultimateColumn;
    }
    nlinkArr.alignRight;
    foreach(i, el; nlinkArr) fileItems[i].nlink = el;
    ownerArr.alignLeft;
    foreach(i, el; ownerArr) fileItems[i].owner = el;
    groupArr.alignLeft;
    foreach(i, el; groupArr) fileItems[i].group = el;
    sizeArr.alignRight;
    foreach(i, el; sizeArr) fileItems[i].size = el;
    dayArr.alignRight;
    foreach(i, el; dayArr) fileItems[i].day = el;
    penColumnArr.alignRight;
    foreach(i, el; penColumnArr) fileItems[i].penultimateColumn = el;
    
    foreach(item; fileItems) {
        item.print();
    }
}


enum Alignment {
    left,
    right
}


void alignLines(ref string[] lines, Alignment alignment) {
    size_t maxLength = 0;
    foreach(l; lines) {
        size_t nsymbols = l.to!dstring.length;
        if (nsymbols > maxLength) maxLength = nsymbols;
    }
    foreach(ref l; lines) {
        size_t nsymbols = l.to!dstring.length;
        char[] spaces = new char[maxLength - nsymbols];
        foreach(ref el; spaces) el = ' ';
        final switch(alignment) {
            case Alignment.left: l ~= spaces.idup; break;
            case Alignment.right: l = spaces.idup ~ l;
        }
    }
}


void alignRight(ref string[] lines) {
    alignLines(lines, Alignment.right);
}


void alignLeft(ref string[] lines) {
    alignLines(lines, Alignment.left);
}


string[] getFileList(string catalog, bool hidden = false) {
    return std.file.dirEntries(catalog, SpanMode.shallow)
        .map!(a => a.name)
        .filter!(a => hidden || !baseName(a).startsWith("."))
        .array;
}


/*******************************************************************************
 * The function returns file permissions a string form (rwxrwxrwx)
 */
string makeUnixFileModeLine(FileStat st) {
    char[10] permissions;
    permissions[0] = cast(char)st.type;
    foreach(shift, rwxMask; [0: S_IRWXU, 3: S_IRWXG, 6: S_IRWXO]) {
        auto rwxMode = (st.mode & rwxMask) >> (6-shift);
        permissions[shift+1] = (rwxMode & 4) ? 'r' : '-';
        permissions[shift+2] = (rwxMode & 2) ? 'w' : '-';
        permissions[shift+3] = (rwxMode & 1) ? 'x' : '-';
    }
    return permissions.idup;
}


/*******************************************************************************
 * Portable structure for storing and retrieving file information
 * on UNIX-like systems.
 */
struct FileStat {
    /// ID of device containing file 
    ulong dev;
    /// Inode number
    ulong inode;
    /// File type and mode
    uint mode;
    /// Number of hard links
    ulong nlink;
    /// User ID of owner
    uint uid;
    /// Group ID of owner
    uint gid;
    /// Device ID (if special file)
    ulong rdev;
    /// Total size, in bytes
    ulong size;
    /// Blocksize for file system I/O
    long blocksize;
    /// Number of 512B blocks allocated
    long blocks;
    /// Time of last access
    SysTime atime;
    /// Time of last modification
    SysTime mtime;
    /// Time of last status change
    SysTime ctime;


    /***************************************************************************
     * The constructor created object with file information by file path.
     */
    this(string filepath) {
        stat_t st;
        lstat(filepath.toStringz, &st);
        dev = st.st_dev;
        inode = st.st_ino;
        mode = st.st_mode;
        nlink = st.st_nlink;
        uid = st.st_uid;
        gid = st.st_gid;
        rdev = st.st_rdev;
        size = st.st_size;
        blocksize = st.st_blksize;
        blocks = st.st_blocks;
        atime = SysTime.fromUnixTime(st.st_atime);
        mtime = SysTime.fromUnixTime(st.st_mtime);
        ctime = SysTime.fromUnixTime(st.st_ctime);
    }

    /***************************************************************************
     * Returns user name of owner.
     */
    @property string user() {
        return getNameByUID(uid);
    }

    /***************************************************************************
     * Returns group name of owner.
     */
    @property string group() {
        return getNameByGID(gid);
    }
    
    /***************************************************************************
     * Returns file type.
     */
    @property FileType type() {
        switch(this.mode & S_IFMT) {
            case S_IFSOCK: return FileType.socket;
            case S_IFLNK:  return FileType.symlink;
            case S_IFREG:  return FileType.regularFile;
            case S_IFBLK:  return FileType.blockDevice;
            case S_IFDIR:  return FileType.directory;
            case S_IFCHR:  return FileType.charDevice;
            case S_IFIFO:  return FileType.pipe;
            default:       return FileType.wrongType;
        }
    }
}


enum FileType {
    socket = 's',
    symlink = 'l',
    regularFile = '-',
    blockDevice = 'b',
    directory = 'd',
    charDevice = 'c',
    pipe = 'p',
    wrongType = 'z'
}


string getNameByUID(uint uid) {
    auto p = getpwuid(uid);
    if (p is null) {
        return uid.to!string;
    }
    return (*p).pw_name.fromStringz.to!string;
}


string getNameByGID(uint gid) {
    auto p = getgrgid(gid);
    if (p is null) {
        return gid.to!string;
    }
    return (*p).gr_name.fromStringz.to!string;
}
